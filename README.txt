$Id 
GMap Extra Markers
-----------------------------
This module gives the ability to add markers from a views to a GMap markers.

To use:
1. This module must be used a page that has a Gmap Map on it.  It doesn't create an actual map.
2. Create a view:
    a. It must have the longitude,latitude, and node type  fields
    b. It should have block view and not a page view.
    c. Set view type to "GMap: Extra Markers"
3. Place the view on page that has GMap(via blocks or panels).

The markers are added to GMap markers collection via Javascript after the page is loaded.

Use can use a GMAp Macro on the view control the settings for the markers. 
GMap Macro settings for the general Map will not apply. 
If the setting "rmtcallback" is set for a map using this module then the markers will have a callback "rmt" setting of: "$node_type/$node_id".
The actual url of the "rmtcallback" setting will be ignored. To set the url use a GMap Macro on original GMap.

***********************


