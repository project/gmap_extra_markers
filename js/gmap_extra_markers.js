Drupal.gmap_extra_markers = new function(){
	this.getExtraMarkers = function(){
		extraMarkers = new Array();
		for ( var mapid in Drupal.settings.gmap_extra_markers){
			extraMarkers = extraMarkers.concat(Drupal.settings.gmap_extra_markers[mapid]);
		}
		return extraMarkers;
	};
};
Drupal.gmap.addHandler('gmap', function (elem) {
  var obj = this;

  obj.bind("boot",function(){
	  
	  obj.vars.markers = obj.vars.markers.concat(Drupal.gmap_extra_markers.getExtraMarkers());
  });

});